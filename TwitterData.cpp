#include "TwitterData.hpp"

/*
TwitterData::TwitterData()
{

}

TwitterData::TwitterData(TwitterData &obj)
{
	setUserName(obj.getUserName());

	setActualName(obj.getActualName());

	setEmail(obj.getEmail());

	setCategory(obj.getCategory());

	setNumTweets(obj.getNumTweets());
}

TwitterData::TwitterData(TwitterData &&obj)
{
	mpUserName = &(obj.getUserName());

	mpActualName = &(obj.getActualName());

	mpEmail = &(obj.getActualName());

	mpCategory = &(obj.getCategory());

	mpNumTweets = &(obj.getNumTweets());
}

TwitterData& TwitterData::operator=(TwitterData &rhs)
{
	setUserName(rhs.getUserName());

	setActualName(rhs.getActualName());

	setEmail(rhs.getEmail());

	setCategory(rhs.getCategory());

	setNumTweets(rhs.getNumTweets());
}

TwitterData& TwitterData::operator=(TwitterData &&rhs)
{
	mpUserName = &(rhs.getUserName());

	mpActualName = &(rhs.getActualName());

	mpEmail = &(rhs.getActualName());

	mpCategory = &(rhs.getCategory());

	mpNumTweets = &(rhs.getNumTweets());
}
*/

std::string TwitterData::getUserName() const
{
    return *(this->mpUserName);
}

std::string TwitterData::getActualName() const
{
    return *(this->mpActualName);
}

std::string TwitterData::getEmail() const
{
    return *(this->mpEmail);
}

std::string TwitterData::getCategory() const
{
    return *(this->mpCategory);
}

int TwitterData::getNumTweets() const
{
    return *(this->mpNumTweets);
}

void TwitterData::setUserName(const std::string& newUserName)
{
    // do we need to allocate space for a std::string here?

	// Without the default constructor, yes

    mpUserName = new std::string;

    *mpUserName = newUserName;

}

void TwitterData::setActualName(const std::string& newActualName)
{
    // do we need to allocate space for a std::string here?

	// Without the default constructor, yes

    mpActualName = new std::string;

    *mpActualName = newActualName;
}

void TwitterData::setEmail(const std::string& newEmail)
{
    // do we need to allocate space for a std::string here?

	// Without the default constructor, yes

    mpEmail = new std::string;

    *mpEmail = newEmail;
}

void TwitterData::setCategory(const std::string& newCategory)
{
    // do we need to allocate space for a std::string here?

	// Without the default constructor, yes

    mpCategory = new std::string;

    *mpCategory = newCategory;
}

void TwitterData::setNumTweets(const int& newNumTweets)
{
    // do we need to allocate space for an int here?

	// Without the default constructor, yes

    mpNumTweets = new int;

    *mpNumTweets = newNumTweets;
}

std::ostream& operator<<(std::ostream& filestream, TwitterData object)
{
    filestream << object.getUserName() << ",\"" << object.getActualName() << "," << object.getEmail() << "," << object.getNumTweets() << "," << object.getCategory() << "\"";

    return filestream;
}
