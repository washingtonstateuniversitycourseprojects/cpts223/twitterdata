/*
    What did you use as a key and why: 	I used the username of the profile
										as my key. I did this because I could see that
										all of the values would be unique and I wanted an
										easy way to visualize the corresponding of the
										profiles to their keys/values to their keys.

    Worst case Big-O insert with key: 	The worst case insertion using a key in a red-
										black tree would be O(logn)

    Worst case Big-O retrieve with key:	The worst case retrieval using a key in a red-
										black tree would be O(logn)

    Worst case Big-O delete with key:	The worst case deletion using a key in a red-
										black tree would be O(logn)

    Worst case Big-O iterate through map: The worst case for iterating through an entire
										map using a red-black tree is O(n)

	Worst case Big-O delete with value:	The worst case deletion using a value in a red-
										black tree would be O(n)

	When and why should we use a map:	A map should be used when there is an obvious and
										easy to implement key, as in a key that is
										associated with each value being inserted into the
										map and will be easy to work with if there are
										repeats/guarentees no repeats. The key is what
										allows faster access and operations, so it's 
										important that it can be utilized.

	Is a map ideal to retrieve most viewed category: No, assuming it's being used for
										advertising, a container sorted by most viewed
										category would be ideal, but with limited categories,
										using category as a key in a map wouldn't work,
										there would be too many repeats of keys. A more ideal
										structure would be a two dimensional array containing
										categories in the first degree and profiles in the second
										degree. A map WOULD work if the objects being stored in
										the map were most viewed category keys and values that
										contained multiple profiles, but that wouldn't be an
										efficient method of storage for access/insertion/deletion.
*/

#include <map>
#include "TwitterData.hpp"
#include <fstream>

int main(int argc, char* argv[])
{
    // we need a map to store our key-value pairs
    // std::map<keyType, ValueType>; What should the key be? What about the value?
	// using an int corresponding to username for key and TwitterData for value
    //max is 11x127 - 1 = 1396, so max key won't be very large with this data

    std::map<std::string,TwitterData> twitterDataMap;

    std::string throwaway;

    std::ifstream fileStream;

    fileStream.open("TwitterAccounts.csv");

    while (!fileStream.eof())
    {
        std::string newData;

        std::string newName = "";

        std::string keyString = "";

        getline(fileStream, newData, ',');

        if ((newData == "") || (newData == "\n"))
        {
            getline(fileStream, newData, ',');

            if ((newData == "") || (newData == "\n"))
            {
                break;
            }
        }

        TwitterData newTwitterData;

        newTwitterData.setUserName(newData);

		keyString = newData;

        getline(fileStream, newData, '\"');

        getline(fileStream, newData, ',');

        newName.append(newData);

        newName.append(",");

        getline(fileStream, newData, ',');

        newName.append(newData);

        newTwitterData.setActualName(newName);

        getline(fileStream, newData, ',');

        newTwitterData.setEmail(newData);

        getline(fileStream, newData, ',');

        if (newData != "NumTweets")
        {
            newTwitterData.setNumTweets(stoi(newData));
        }
        else
        {
            newTwitterData.setNumTweets(0000);
        }

        getline(fileStream, newData, '\"');

        newTwitterData.setCategory(newData);

        std::pair<std::string,TwitterData> newPair = { keyString, newTwitterData };

        twitterDataMap.insert(newPair);

        getline(fileStream, newData);
    }

    std::cout << "Before any deletions:\n" << std::endl;

    std::map<std::string,TwitterData>::iterator it = twitterDataMap.begin();

    while (it != twitterDataMap.end())
    {
        std::cout << it->first << ": " << it->second << "\n" << std::endl;

        it++;
    }

    std::cout << "\n" << std::endl;

    std::string deleteAccount = "savage1";

    twitterDataMap.erase(deleteAccount);

    std::cout << "After deleting \"savage1\":\n" << std::endl;

    it = twitterDataMap.begin();

    while (it != twitterDataMap.end())
    {
        std::cout << it->first << ": " << it->second << "\n" << std::endl;

        it++;
    }

    std::cout << "\n" << std::endl;

    it = twitterDataMap.begin();

    while (it != twitterDataMap.end())
    {
        if (it->second.getActualName() == "Smith,Rick")
        {
            break;
        }

        it++;
    }

    twitterDataMap.erase(it);

    std::cout << "After deleting \"Smith,Rick\":\n" << std::endl;

    it = twitterDataMap.begin();

    while (it != twitterDataMap.end())
    {
        std::cout << it->first << ": " << it->second << "\n" << std::endl;

        it++;
    }

    std::cout << "\n" << std::endl;

    return 0;
}
