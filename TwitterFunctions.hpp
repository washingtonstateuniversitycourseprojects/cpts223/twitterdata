#ifndef TWITTER_FUNCTIONS_H
#define TWITTER_FUNCTIONS_H

#include "TwitterData.hpp"
#include <map>

void readInData(map<int,TwitterData> *&twitterMap, std::ifstream &fileStream);

void printData(map<int,TwitterData>);


#endif