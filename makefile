prog: main.o TwitterData.o
	g++ main.o TwitterData.o -o runProg

main.o: main.cpp TwitterData.hpp
	g++ -c -g -Wall -std=c++11 main.cpp

TwitterData.o: TwitterData.cpp TwitterData.hpp
	g++ -c -g -Wall -std=c++11 TwitterData.cpp

clean:
	-rm *.o

run: @./runProg